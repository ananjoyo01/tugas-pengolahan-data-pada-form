<?php
if(isset($_POST['submit'])) {
    $nama = $_POST['nama'];
    $mapel = $_POST['mapel'];
    $uts = $_POST['uts'];
    $uas = $_POST['uas'];
    $tgs = $_POST['tgs'];
    
    //Mencari nilai akhir
    $nilai_akhir = $uts * 0.35 + $uas * 0.5 + $tgs * 0.15;

    //Menampilkan Grade dari nilai akhir
    if ($nilai_akhir>=90) {
        $grade = "A";
    } elseif ($nilai_akhir>=70) {
        $grade = "B";
    } elseif ($nilai_akhir>=50) {
        $grade = "C";
    } else{
        $grade = "D";
    }
}
?>

<?php if (isset($_POST['submit'])) : ?> 

            <div class="row justify-content-center">
            <div class="col-8 border rounded-2 border-dark mt-8 p-8" style="background-color: green">
            <div class="alert alert-primary" >

                Nama                :<?php echo $nama ?> <br>
                Mata Pelajaran      :<?php echo $mapel ?> <br>
                Nilai UTS           :<?php echo $uts ?> <br>
                Nilai UAS           :<?php echo $uas ?> <br>
                Tugas               :<?php echo $tgs ?> <br>
                Grade               :<?php echo $grade ?> <br>
            </div>
            </div>
        </div>
        <?php endif ; ?>
        </div>
    </div>
